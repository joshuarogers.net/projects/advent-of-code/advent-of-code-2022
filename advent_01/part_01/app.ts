import { readFileSync } from 'fs';
import { sumBy } from 'lodash';

const input = readFileSync("input.txt")
    .toString()
    .split("\n");

const splitBy = <T>(input: T[], shouldSplit: (x: T) => boolean) => {
    return input.reduce((groups, x) => {
        if (shouldSplit(x))
            groups.push([]);
        else
            groups[groups.length - 1].push(x);

        return groups;
    }, [[]] as T[][]);
};

const partitions = splitBy(input, x => x === "");
const caloriesPerElf = partitions.map(partition => sumBy(partition, x => parseInt(x)));

console.log(Math.max(...caloriesPerElf));
