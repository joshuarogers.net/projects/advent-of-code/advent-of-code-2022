import { readFileSync } from 'fs';
import { chain, sumBy } from 'lodash';

const input = readFileSync("input.txt")
    .toString()
    .split("\n");

const splitBy = <T>(input: T[], shouldSplit: (x: T) => boolean) => {
    return input.reduce((groups, x) => {
        if (shouldSplit(x))
            groups.push([]);
        else
            groups[groups.length - 1].push(x);

        return groups;
    }, [[]] as T[][]);
};

const partitions = splitBy(input, x => x === "");
const caloriesOfTopThreeElves = chain(partitions)
    .map(partition => sumBy(partition, x => parseInt(x)))
    .orderBy(x => x, "desc")
    .take(3)
    .sum()
    .value();

console.log(caloriesOfTopThreeElves);
