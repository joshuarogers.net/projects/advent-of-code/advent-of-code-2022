import { readFileSync } from 'fs';
import { sum } from 'lodash';

type SecretSymbols = 'A' | 'B' | 'C' | 'X' | 'Y' | 'Z';
type Choices = 'Paper' | 'Rock' | 'Scissors';
type Outcome = 'Win' | 'Lose' | 'Draw';

const decoder: Record<SecretSymbols, Choices> = {
    'A': 'Rock',
    'B': 'Paper',
    'C': 'Scissors',
    'X': 'Rock',
    'Y': 'Paper',
    'Z': 'Scissors'
};

const winningMoves: Record<Choices, Choices> = {
    'Paper': 'Rock',
    'Rock': 'Scissors',
    'Scissors': 'Paper'
};

const choicePoints: Record<Choices, number> = {
    'Rock': 1,
    'Paper': 2,
    'Scissors': 3
};

const outcomePoints: Record<Outcome, number> = {
    'Win': 6,
    'Draw': 3,
    'Lose': 0
};

type Turn = { opponent: Choices, self: Choices };


const getOutcome = (turn: Turn): Outcome => {
    if (turn.opponent === turn.self)
        return 'Draw';

    return winningMoves[turn.self] === turn.opponent
        ? 'Win'
        : 'Lose';
}

const input = readFileSync("input.txt")
    .toString()
    .trim()
    .split("\n");

const strategyGuide: Turn[] = input
    .map(x => x.split(' ') as [SecretSymbols, SecretSymbols])
    .map(([opponentCode, selfCode]) => ({ opponent: decoder[opponentCode], self: decoder[selfCode] }));

const pointsByTurn = strategyGuide
    .map(turn => outcomePoints[getOutcome(turn)] + choicePoints[turn.self]);

console.log(sum(pointsByTurn));
