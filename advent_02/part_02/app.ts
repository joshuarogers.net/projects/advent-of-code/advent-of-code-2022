import { readFileSync } from 'fs';
import { sum } from 'lodash';

type EncryptedChoice = 'A' | 'B' | 'C';
type EncryptedOutcome = 'X' | 'Y' | 'Z';
type Choices = 'Paper' | 'Rock' | 'Scissors';
type Outcome = 'Win' | 'Lose' | 'Draw';

const choiceDecoder: Record<EncryptedChoice, Choices> = {
    'A': 'Rock',
    'B': 'Paper',
    'C': 'Scissors'
};

const outcomeDecoder: Record<EncryptedOutcome, Outcome> = {
    'X': 'Lose',
    'Y': 'Draw',
    'Z': 'Win'
};

const winningMovesReverseLookup: Record<Choices, Choices> = {
    'Rock': 'Paper',
    'Scissors': 'Rock',
    'Paper': 'Scissors'
};

const losingMovesReverseLookup: Record<Choices, Choices> = {
    'Paper': 'Rock',
    'Rock': 'Scissors',
    'Scissors': 'Paper'
};

const choicePoints: Record<Choices, number> = {
    'Rock': 1,
    'Paper': 2,
    'Scissors': 3
};

const outcomePoints: Record<Outcome, number> = {
    'Win': 6,
    'Draw': 3,
    'Lose': 0
};

type Turn = { opponent: Choices, outcome: Outcome };


const derivePreferredMove = (turn: Turn): Choices => {
    if (turn.outcome === 'Draw')
        return turn.opponent;

    return turn.outcome === 'Win'
        ? winningMovesReverseLookup[turn.opponent]
        : losingMovesReverseLookup[turn.opponent];
}

const input = readFileSync("input.txt")
    .toString()
    .trim()
    .split("\n");

const strategyGuide: Turn[] = input
    .map(x => x.split(' ') as [EncryptedChoice, EncryptedOutcome])
    .map(([opponentCode, selfCode]) => ({ opponent: choiceDecoder[opponentCode], outcome: outcomeDecoder[selfCode] }));

const pointsByTurn = strategyGuide
    .map(turn => outcomePoints[turn.outcome] + choicePoints[derivePreferredMove(turn)]);

console.log(sum(pointsByTurn));
