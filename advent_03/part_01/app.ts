import { readFileSync } from 'fs';
import { chunk, flatten, fromPairs, intersection, sum } from 'lodash';

type RucksackCompartment = string[];
type Rucksack = [compartment1: RucksackCompartment, compartment2: RucksackCompartment];

const input = readFileSync("input.txt")
    .toString()
    .trim()
    .split("\n");

const rucksacks:Rucksack[] = input
    .map(x => x.split(''))
    .map(x => chunk(x, x.length / 2) as [RucksackCompartment, RucksackCompartment]);

const misplacedItems = flatten(rucksacks.map(rucksack => intersection(...rucksack)));

const itemValues = fromPairs("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    .split('')
    .map((x, i) => [x, i + 1]));

const misplacedItemValues = misplacedItems
    .map(x => itemValues[x])

console.log(sum(misplacedItemValues));