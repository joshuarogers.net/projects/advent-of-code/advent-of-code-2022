import { readFileSync } from 'fs';
import { chunk, flatten, fromPairs, intersection, sum } from 'lodash';

type Rucksack = string[];

const input = readFileSync("input.txt")
    .toString()
    .trim()
    .split("\n");

const rucksacks:Rucksack[] = input
    .map(x => x.split(''));

const rucksackGroups = chunk(rucksacks, 3);
const rucksackGroupIds = flatten(rucksackGroups.map(x => intersection(...x)));

const itemValues = fromPairs("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    .split('')
    .map((x, i) => [x, i + 1]));
const groupValues = rucksackGroupIds.map(x => itemValues[x]);

console.log(sum(groupValues));
