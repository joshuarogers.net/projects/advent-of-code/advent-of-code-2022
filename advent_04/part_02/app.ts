import { readFileSync } from 'fs';
import { intersection, range } from 'lodash';

type WorkAssignment = number[];
type Team = [member1: WorkAssignment, member2: WorkAssignment];

const input = readFileSync("input.txt")
    .toString()
    .trim()
    .split("\n");

const teams: Team[] = input.map(teamAssignment => {
    const [elf1min, elf1max, elf2min, elf2max] = teamAssignment
        .split(/[-,]/)
        .map(x => parseInt(x));
    return [range(elf1min, elf1max + 1), range(elf2min, elf2max + 1)];
});

const teamsWithOverlap = teams.filter(([assignmemnt1, assignment2]) => intersection(assignmemnt1, assignment2).length > 0);
console.log(teamsWithOverlap.length);