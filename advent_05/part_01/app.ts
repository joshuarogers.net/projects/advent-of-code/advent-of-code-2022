import { readFileSync } from 'fs';
import { range, tail } from 'lodash';

interface Instruction {
    count: number;
    from: number;
    to: number;
}

const input = readFileSync("input.txt")
    .toString()
    .trimEnd()
    .split("\n");

const separatorIndex = input.indexOf("");

const stacks = range(1, input[0].length, 4)
    .map(x => range(0, separatorIndex - 1)
        .map(y => input[y][x])
        .filter(x => x !== ' '));

const instructions: Instruction[] = input
    .slice(separatorIndex + 1)
    .map(x => {
        const parsedValues = x.match(/move (\d+) from (\d+) to (\d+)/);
        const [count, from, to] = tail(parsedValues).map(i => parseInt(i));
        return { count, from, to };
    });

const moveCrate = (from: number, to: number) =>
    stacks[to - 1].unshift(stacks[from - 1].shift()!);

const applyInstruction = (instruction: Instruction) => {
    for (var i = 0; i < instruction.count; i++)
        moveCrate(instruction.from, instruction.to);
}

for (const instruction of instructions)
    applyInstruction(instruction)

console.log(stacks.map(x => x[0]).join(''));