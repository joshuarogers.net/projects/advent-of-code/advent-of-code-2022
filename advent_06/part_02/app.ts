import { readFileSync } from 'fs';
import { uniq } from 'lodash';

const input = readFileSync("input.txt")
    .toString()
    .trimEnd()
    .split('');

const headerSize = 14;
const isPacketStart = (index: number) => {
    const potentialMarker = input.slice(index - headerSize, index);
    return uniq(potentialMarker).length === headerSize;
};

for (let index = 3; index < input.length; index++) {
    if (isPacketStart(index)) {
        console.log(index);
        break;
    }
}
