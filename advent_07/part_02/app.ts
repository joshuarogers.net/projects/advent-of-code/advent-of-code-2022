import { readFileSync } from 'fs';
import { chain } from 'lodash';

interface DirectoryInode {
    type: 'directory';
}

interface FileInode {
    type: 'file';
    bytes: number;
}

type Inode = DirectoryInode | FileInode;
type Filesystem = Record<string, Inode>;

const createTerminal = (filesystem: Filesystem) => {
    let workingDirectory: string = "";

    const normalizePath = (path: string): string => {
        const resolveAbsolutePath = (path: string): string => {
            if (path.startsWith('/'))
                return path;

            return path === '..'
                ? workingDirectory.substring(0, workingDirectory.lastIndexOf('/'))
                : `/${workingDirectory}/${path}`;
        };
        
        return resolveAbsolutePath(path)
            .replace(/\/+/g, '/')
            .replace(/\/$/, '');
    };
    

    const pwd = (): string => workingDirectory;

    const cd = (path: string): void => {
        workingDirectory = normalizePath(path);
    };

    const mkdir = (path: string): void => {
        filesystem[normalizePath(path)] = { type: 'directory' };
    };

    const dd = (path: string, bytes: number): void => {
        filesystem[normalizePath(path)] = { type: 'file', bytes };
    };

    const find = (path: string, type: 'directory' | 'file'): string[] => {
        const pathPrefix = normalizePath(`${path}/`);

        return chain(filesystem)
            .toPairs()
            .filter(([_, inode]) => inode.type === type)
            .map(([absolutePath, _]) => absolutePath)
            .filter(x => x.startsWith(`${pathPrefix}/`))
            .sort()
            .value();
    };

    const du = (path: string): number => {
        return chain(find(path ?? pwd(), 'file'))
            .map(x => filesystem[x] as FileInode)
            .map(x => x.bytes)
            .sum()
            .value();
    };

    const ls = (path: string) => {
        const pathPrefix = normalizePath(`${path ?? pwd()}/`);

        return chain(filesystem)
            .toPairs()
            .filter(([absolutePath, _]) => absolutePath.startsWith(pathPrefix))
            .filter(([absolutePath, _]) => absolutePath.lastIndexOf('/') === pathPrefix.length)
            .map(([absolutePath, inode]) => ({path: absolutePath, ...inode}))
            .sortBy(["type", "path"])
            .value();
    }

    return { cd, dd, du, find, ls, mkdir, pwd };
};


interface TerminalLog {
    command: string[];
    response: string[];
}

const input = readFileSync("input.txt").toString();
const terminalLogs: TerminalLog[] = input
    .split("\$")
    .filter(x => x)
    .map(x => {
        const [commandLine, ...response] = x.split('\n');
        return { 
            command: commandLine.trim().split(' '),
            response: response.filter(x => x)
        };
    });

const { cd, dd, du, find, ls, mkdir, pwd } = createTerminal({});

for (const terminalLog of terminalLogs) {
    const { command: [commandText, commandArg], response } = terminalLog;
    if (commandText === 'cd')
        cd(commandArg);
    else if (commandText === 'ls') {
        for (const [sizeOrDir, name] of response.map(x => x.split(' '))) {
            if (sizeOrDir === 'dir')
                mkdir(name);
            else
                dd(name, parseInt(sizeOrDir));
        }
    }
}

const filesystemBytesTotal = 70000000;
const filesystemBytesUsed = du('/');
const filesystemBytesFree = filesystemBytesTotal - filesystemBytesUsed;

const updateBytesRequirement = 30000000;

console.log(chain(find('/', 'directory'))
    .map(du)
    .orderBy(x => x, 'asc')
    .filter(x => x + filesystemBytesFree >= updateBytesRequirement)
    .first()
    .value());
