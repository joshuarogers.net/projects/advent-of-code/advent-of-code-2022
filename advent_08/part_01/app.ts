import { readFileSync } from 'fs';
import { range } from 'lodash';

type TreeHeightMap = number[][];
type TreeVisibilityMap = boolean[][];

const input = readFileSync("input.txt").toString();

const treeHeightMap: TreeHeightMap = input
    .trim()
    .split('\n')
    .map(x => x.split('').map(y => parseInt(y)));

const xMax = treeHeightMap[0].length;
const yMax = treeHeightMap.length;

const checkTreeVisibility = (treeHeightMap: TreeHeightMap, [x, y]: [number, number]) => {
    const getTreeHeight = (x: number, y: number): number => treeHeightMap[y][x];
    const isTallestTree = (thisTreeHeight: number, otherTreeHeights: number[]) => Math.max(...otherTreeHeights) < thisTreeHeight;

    const thisTreeHeight = getTreeHeight(x, y);
    const otherTreeHeights = [
        range(0, x).map(x => getTreeHeight(x, y)),
        range(x + 1, xMax).map(x => getTreeHeight(x, y)),
        range(0, y).map(y => getTreeHeight(x, y)),
        range(y + 1, yMax).map(y => getTreeHeight(x, y))
    ];

    return otherTreeHeights.some(ray => isTallestTree(thisTreeHeight, ray));
};

const treeVisibilityMap: TreeVisibilityMap = range(0, yMax)
    .map(y => range(0, xMax)
        .map(x => checkTreeVisibility(treeHeightMap, [x, y])));

console.log(treeVisibilityMap
    .flatMap(x => x)
    .filter(x => x)
    .length);
