import { readFileSync } from 'fs';
import { chain, range, takeWhile } from 'lodash';

type TreeHeightMap = number[][];
type TreeVisibilityMap = boolean[][];

const input = readFileSync("input.txt").toString();

const treeHeightMap: TreeHeightMap = input
    .trim()
    .split('\n')
    .map(x => x.split('').map(y => parseInt(y)));

const xMax = treeHeightMap[0].length;
const yMax = treeHeightMap.length;

const checkTreeScenicScore = (treeHeightMap: TreeHeightMap, [x, y]: [number, number]) => {
    const getTreeHeight = (x: number, y: number): number => treeHeightMap[y][x];

    const thisTreeHeight = getTreeHeight(x, y);

    // This considers the other trees in the grid of (1, 1) to (xMax - 1, yMax - 1), excluding the trees on
    // the perimeter as they are guaranteed to be the furtest trees that we can view. Regardless of whether
    // they are taller, shorter, or the same, we could still see them if we could see the tree one layer closer.
    // because of that, there's no need to test whether we can see it and also no need for us to add special
    // case logic, as perimeter trees are the only ones whether you are not able to see the side of another
    // tree, after passing a shorter tree.
    const otherTreeHeights = [
        // The left and top checks include a reverse so that we are checking closest trees first, as that
        // requires the indices to be descending.
        range(1, x).reverse().map(x => getTreeHeight(x, y)),
        range(1, y).reverse().map(y => getTreeHeight(x, y)),
        range(x + 1, xMax - 1).map(x => getTreeHeight(x, y)),
        range(y + 1, yMax - 1).map(y => getTreeHeight(x, y))
    ];

    const rayDistances = otherTreeHeights
        .map(ray => takeWhile(ray, otherTreeHeight => thisTreeHeight > otherTreeHeight))
        // Adds one to each ray, since we can see the side of the next tree.
        .map(ray => ray.length + 1);
    
    return rayDistances.reduce((x, y) => x * y, 1);
};

const highestScenicScore: number = chain(range(1, yMax - 1))
    .flatMap(y => range(1, xMax - 1)
        .map(x => checkTreeScenicScore(treeHeightMap, [x, y])))
    .orderBy(x => x, 'desc')
    .first()
    .value();

console.log(highestScenicScore);
