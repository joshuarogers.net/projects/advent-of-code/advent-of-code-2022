import { readFileSync } from 'fs';
import { chain } from 'lodash';

type Direction = 'U' | 'D' | 'L' | 'R';
type Coordinate = [number, number];

interface Rope {
    head: Coordinate;
    tail: Coordinate;
}

interface RopeMovement {
    direction: Direction;
    distance: number;
}

const input = readFileSync("input.txt")
    .toString()
    .trim();

const ropeMovements: RopeMovement[] = input
    .split('\n')
    .map(x => {
        const [direction, distance] = x.split(' ');
        return {
            direction: direction as Direction,
            distance: parseInt(distance)
        };
    });

const headMovementRule: Record<Direction, (head: Coordinate) => Coordinate> = {
    'U': ([x, y]) => [x, y + 1],
    'D': ([x, y]) => [x, y - 1],
    'L': ([x, y]) => [x - 1, y],
    'R': ([x, y]) => [x + 1, y]
};

const tailSnappingRule: Record<Direction, (head: Coordinate) => Coordinate> = {
    'U': headMovementRule['D'],
    'D': headMovementRule['U'],
    'L': headMovementRule['R'],
    'R': headMovementRule['L']
};

const applyMovement = ({head: [originalHeadX, originalHeadY], tail: [originalTailX, originalTailY]}: Rope, direction: Direction): Rope => {
    const [newHeadX, newHeadY] = headMovementRule[direction]([originalHeadX, originalHeadY]);

    const snappingThreshhold = 2;
    const needsTailSnapping = Math.abs(newHeadX - originalTailX) === snappingThreshhold
        || Math.abs(newHeadY - originalTailY) === snappingThreshhold;

    const [newTailX, newTailY] = needsTailSnapping
        ? tailSnappingRule[direction]([newHeadX, newHeadY])
        : [originalTailX, originalTailY];

    return {
        head: [newHeadX, newHeadY],
        tail: [newTailX, newTailY]
    };
};

const initialRopeState: Rope = {
    head: [0, 0],
    tail: [0, 0]
};

const ropeDirectionSteps = ropeMovements.flatMap(x => Array<Direction>(x.distance).fill(x.direction));

let currentRopeState = initialRopeState;
const ropeStates: Rope[] = [initialRopeState];

for (const ropeDirectionStep of ropeDirectionSteps) {
    currentRopeState = applyMovement(currentRopeState, ropeDirectionStep);
    ropeStates.push(currentRopeState);
}

console.log(chain(ropeStates)
    .map(({tail: [x, y]})=> `${x},${y}`)
    .uniq()
    .value()
    .length);