import { readFileSync } from 'fs';
import { chain } from 'lodash';

type Direction = 'U' | 'D' | 'L' | 'R';
type Coordinate = [number, number];
type Rope = Coordinate[];

interface RopeMovement {
    direction: Direction;
    distance: number;
}

const input = readFileSync("input.txt")
    .toString()
    .trim();

const ropeMovements: RopeMovement[] = input
    .split('\n')
    .map(x => {
        const [direction, distance] = x.split(' ');
        return {
            direction: direction as Direction,
            distance: parseInt(distance)
        };
    });

const projectCoordinate = (direction: Direction, [x, y]: Coordinate): Coordinate => {
    switch (direction) {
        case 'U': return [x, y + 1];
        case 'D': return [x, y - 1];
        case 'L': return [x - 1, y];
        case 'R': return [x + 1, y];
    }
};

const snapCoordinate = ([currentX, currentY]: Coordinate, [parentX, parentY]: Coordinate): Coordinate => {
    const dx = parentX - currentX;
    const dy = parentY - currentY;

    // If snapping is neccessary, the current coordinate will be at a known location relative to the parent.
    // If snapping is unnecessary, then the coordinate will not be changed.
    let coordinate: Coordinate = [parentX, parentY];
    if (dx === -2)
        coordinate = projectCoordinate('R', coordinate);
    if (dx === 2)
        coordinate = projectCoordinate('L', coordinate);
    if (dy === -2)
        coordinate = projectCoordinate('U', coordinate);
    if (dy === 2)
        coordinate = projectCoordinate('D', coordinate);
    return Math.abs(dx) <= 1 && Math.abs(dy) <= 1
        ? [currentX, currentY]
        : coordinate;
};

const applyMovement = ([head, ...body]: Rope, direction: Direction): Rope => {
    const newHead = projectCoordinate(direction, head);

    let previousBodyPart = newHead;
    const newSnake = [ newHead ];
    for (const bodyPart of body) {
        const newBodyPart = snapCoordinate(bodyPart, previousBodyPart);

        newSnake.push(newBodyPart);
        previousBodyPart = newBodyPart;
    }

    return newSnake;
};

const initialRopeState: Rope = Array(10).fill([0, 0]);

const ropeDirectionSteps = ropeMovements.flatMap(x => Array<Direction>(x.distance).fill(x.direction));

let currentRopeState = initialRopeState;
const ropeStates: Rope[] = [initialRopeState];

for (const ropeDirectionStep of ropeDirectionSteps) {
    currentRopeState = applyMovement(currentRopeState, ropeDirectionStep);
    ropeStates.push(currentRopeState);
}

console.log(chain(ropeStates)
    .map(x => x[x.length - 1])
    .map(([x, y]) => `${x},${y}`)
    .uniq()
    .value()
    .length);
