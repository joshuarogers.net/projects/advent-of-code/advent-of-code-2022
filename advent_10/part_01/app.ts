import { readFileSync } from 'fs';
import { chain, findLast, sum } from 'lodash';

type NoopInstruction = { type: 'noop' };
type AddXInstruction = { type: 'addx', value: number };
type Instruction = NoopInstruction | AddXInstruction;

type CpuState = { cycle: number, register: number };
const createCpu = () => {
    let cycle = 1;
    let register = 1;

    const noop = () => { cycle++; };
    const addx = ({ value }: AddXInstruction) => {
        cycle += 2;
        register += value;
    };

    const instructionSet: Record<Instruction['type'], (instruction: any) => void> = {
        'noop': noop,
        'addx': addx
    };

    return {
        execute: (instruction: Instruction) => instructionSet[instruction.type](instruction),
        getState: (): CpuState => ({ cycle, register })
    };
}

const getRegisterAtTime = (chronologicalCpuStates: CpuState[], time: number) => {
    return findLast(chronologicalCpuStates, x => x.cycle <= time)?.register;
}


const input = readFileSync("input.txt")
    .toString()
    .trim()
    .split('\n')
    .map(x => x.split(' '))
    .map(([instruction, value]): Instruction => instruction === 'noop'
        ? { type: 'noop' }
        : { type: 'addx', value: parseInt(value) }
    );

const cpu = createCpu();
const cpuStates = input.map(x => {
    cpu.execute(x);
    return cpu.getState();
});

const validationTimes = [20, 60, 100, 140, 180, 220];
const signalStrength = chain(validationTimes)
    .map(x => x * getRegisterAtTime(cpuStates, x)!)
    .sum()
    .value();

console.log(signalStrength);
