import { readFileSync } from 'fs';
import { chain, chunk, findLast, last, range, sum } from 'lodash';

type NoopInstruction = { type: 'noop' };
type AddXInstruction = { type: 'addx', value: number };
type Instruction = NoopInstruction | AddXInstruction;

type CpuState = { cycle: number, register: number };
const createCpu = () => {
    let cycle = 1;
    let register = 1;

    const noop = () => { cycle++; };
    const addx = ({ value }: AddXInstruction) => {
        cycle += 2;
        register += value;
    };

    const instructionSet: Record<Instruction['type'], (instruction: any) => void> = {
        'noop': noop,
        'addx': addx
    };

    return {
        execute: (instruction: Instruction) => instructionSet[instruction.type](instruction),
        getState: (): CpuState => ({ cycle, register })
    };
}

const getRegisterAtTime = (chronologicalCpuStates: CpuState[], time: number) => {
    return findLast(chronologicalCpuStates, x => x.cycle <= time)?.register;
}


const input = readFileSync("input.txt")
    .toString()
    .trim()
    .split('\n')
    .map(x => x.split(' '))
    .map(([instruction, value]): Instruction => instruction === 'noop'
        ? { type: 'noop' }
        : { type: 'addx', value: parseInt(value) }
    );

const cpu = createCpu();
const cpuStates = [cpu.getState()].concat(input.map(x => {
    cpu.execute(x);
    return cpu.getState();
}));

const getPixelValue = (id: number): '#' | ' ' => {
    const registerValue = getRegisterAtTime(cpuStates, id + 1)!;
    const horizontalId = id % 40;
    const isBright = Math.abs(registerValue - horizontalId) <= 1;
    
    return isBright ? '#' : ' ';
};

const pixels = chain(range(0, 240))
    .map(getPixelValue)
    .chunk(40)
    .map(x => chunk(x, 5).map(y => y.join('')).join(''))
    .value();

console.log(pixels);
