import { readFileSync } from 'fs';
import { fromPairs, keys, orderBy, tail, values, zip } from 'lodash';

interface Item {
    worryLevel: number;
}

interface Monkey {
    id: number;
    inventory: Item[];
    monkeyBehavior: (getMonkeyById: (id: number) => Monkey) => void;
}

const parseMonkeyDefinition = (definition: string): Monkey => {
    const ruleTexts =  definition
        .split('\n')
        .map(x => x.trim());
    
    const parsers = [
        /Monkey (\d+):/,
        /Starting items: (.+)/,
        /Operation: new = old ([*+]) (\d+|old)/,
        /Test: divisible by (\d+)/,
        /If true: throw to monkey (\d+)/,
        /If false: throw to monkey (\d+)/
    ];

    const [[monkeyId], [inventoryText], [opFunc, opValue], [testDivisor], [testPassMonkeyId], [testFailMonkeyId]] = zip(ruleTexts, parsers)
        .map(([ruleText, parser]) => parser!.exec(ruleText!))
        .map(tail);

    const inventory = inventoryText
        .split(',')
        .map(x => parseInt(x.trim()))
        .map(x => ({ worryLevel: x }));

    const worryTransformer = (oldValue: number) => {
        const opNumber = opValue === 'old'
            ? oldValue
            : parseInt(opValue)
        return opFunc === '*'
            ? oldValue * opNumber
            : oldValue + opNumber;
    };

    return {
        id: parseInt(monkeyId),
        inventory,
        monkeyBehavior: (getMonkeyById: (id: number) => Monkey) => {
            while (inventory.length > 0) {
                const { worryLevel: originalWorryLevel } = inventory.shift()!;
                const newWorryLevel = Math.floor(worryTransformer(originalWorryLevel) / 3);
                const targetMonkeyId = (newWorryLevel % parseInt(testDivisor)) === 0
                    ? parseInt(testPassMonkeyId)
                    : parseInt(testFailMonkeyId);
                const targetMonkey = getMonkeyById(targetMonkeyId);
                targetMonkey.inventory.push({ worryLevel: newWorryLevel });
            }
        }
    };
};

const monkeys = readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n\n')
    .map(parseMonkeyDefinition);

const getMonkeyById = (id: number): Monkey => monkeys.find(x => x.id === id)!;

const inspectionCount = fromPairs(monkeys.map(x => ([x.id, 0])));
for (let round = 0; round < 20; round++) {
    for (const monkey of monkeys) {
        inspectionCount[monkey.id] += monkey.inventory.length;
        monkey.monkeyBehavior(getMonkeyById)
    }
}

const [max1, max2, ..._] = orderBy(values(inspectionCount), x => x, 'desc');

console.log(max1 * max2);
