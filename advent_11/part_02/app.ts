import { readFileSync } from 'fs';
import { fromPairs, keys, orderBy, tail, values, zip } from 'lodash';

interface Item {
    worryLevel: number;
}

interface Monkey {
    id: number;
    inventory: Item[];
    monkeyBehavior: (getMonkeyById: (id: number) => Monkey) => void;
}

const parseMonkeyDefinition = (definition: string): Monkey => {
    const ruleTexts =  definition
        .split('\n')
        .map(x => x.trim());
    
    const parsers = [
        /Monkey (\d+):/,
        /Starting items: (.+)/,
        /Operation: new = old ([*+]) (\d+|old)/,
        /Test: divisible by (\d+)/,
        /If true: throw to monkey (\d+)/,
        /If false: throw to monkey (\d+)/
    ];

    const [[monkeyId], [inventoryText], [opFunc, opValue], [testDivisor], [testPassMonkeyId], [testFailMonkeyId]] = zip(ruleTexts, parsers)
        .map(([ruleText, parser]) => parser!.exec(ruleText!))
        .map(tail);

    const inventory = inventoryText
        .split(',')
        .map(x => parseInt(x.trim()))
        .map(x => ({ worryLevel: x }));

    const worryTransformer = (oldValue: number) => {
        const opNumber = opValue === 'old'
            ? oldValue
            : parseInt(opValue)
        return opFunc === '*'
            ? oldValue * opNumber
            : oldValue + opNumber;
    };

    return {
        id: parseInt(monkeyId),
        inventory,
        monkeyBehavior: (getMonkeyById: (id: number) => Monkey) => {
            while (inventory.length > 0) {
                const { worryLevel: originalWorryLevel } = inventory.shift()!;
                const newWorryLevel = worryTransformer(originalWorryLevel);
                const targetMonkeyId = (newWorryLevel % parseInt(testDivisor)) === 0
                    ? parseInt(testPassMonkeyId)
                    : parseInt(testFailMonkeyId);
                const targetMonkey = getMonkeyById(targetMonkeyId);
                targetMonkey.inventory.push({ worryLevel: newWorryLevel });
            }
        }
    };
};

const createWorryWrapper = (worryText: string): (monkeys: Monkey[]) => void => {
    const commonDenominator = worryText
        .split('\n')
        .map(x => /Test: divisible by (\d+)/.exec(x))
        .filter(x => x?.length === 2)
        .map(x => parseInt(x![1]))
        .reduce((x, y) => x * y, 1);

    return (monkeys) => {
        for (const monkey of monkeys)
            for (const item of monkey.inventory)
                item.worryLevel = item.worryLevel % commonDenominator;
    };
};

const input = readFileSync('input.txt')
    .toString()
    .trim();
const monkeys = input.split('\n\n')
    .map(parseMonkeyDefinition);
const worryWrapper = createWorryWrapper(input);

const getMonkeyById = (id: number): Monkey => monkeys.find(x => x.id === id)!;

const inspectionCount = fromPairs(monkeys.map(x => ([x.id, 0])));
for (let round = 0; round < 10000; round++) {
    for (const monkey of monkeys) {
        worryWrapper(monkeys);
        inspectionCount[monkey.id] += monkey.inventory.length;
        monkey.monkeyBehavior(getMonkeyById)
    }
}

const [max1, max2, ..._] = orderBy(values(inspectionCount), x => x, 'desc');

console.log(max1 * max2);
