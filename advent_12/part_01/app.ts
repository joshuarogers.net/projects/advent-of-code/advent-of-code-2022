import { readFileSync } from 'fs';
import { fromPairs, range } from 'lodash';

type Coordinate = [number, number];
type Level = string[];

const heightMapMapping = fromPairs('abcdefghijklmnopqrstuvwxyz'
    .split('')
    .map((x, i) => [x, i]));

// There are two special cases that we can't derive from just an alphabet
heightMapMapping['S'] = heightMapMapping['a'];
heightMapMapping['E'] = heightMapMapping['z'];

const level: Level = readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n');
const maxX = level[0].length;
const maxY = level.length;

const getHeight = ([x, y]: Coordinate): number => heightMapMapping[level[y][x]];

const getHeightDifference = ([srcX, srcY]: Coordinate, [destX, destY]: Coordinate): number => 
getHeight([destX, destY]) - getHeight([srcX, srcY]);

const isCoordinateValid = ([x, y]: Coordinate): boolean =>
    x >= 0 && y >= 0 && x < maxX && y < maxY;

const isMoveValid = ([srcX, srcY]: Coordinate, [destX, destY]: Coordinate): boolean => {
    const maxValidHeightDifference = 1;
    return isCoordinateValid([destX, destY])
        && isCoordinateValid([srcX, srcY])
        && getHeightDifference([srcX, srcY], [destX, destY]) <= maxValidHeightDifference;
};

const findPathDistance = ([startX, startY]: Coordinate, [endX, endY]: Coordinate): number => {
    const coordinateQueue: Coordinate[] = [[startX, startY]];
    const distanceMap = range(0, maxY)
        .map(() => range(0, maxX).map(() => maxX * maxY));
    
    const calculateNeighbors = ([x, y]: Coordinate) => {
        const neighbors: Coordinate[] = [
            [x - 1, y],
            [x + 1, y],
            [x, y - 1],
            [x, y + 1]
        ];

        const reachableNeighbors = neighbors
            .filter(neighbor => isMoveValid([x, y], neighbor));

        const distanceFromStart = distanceMap[y][x] + 1;
        const unreachedNeighbors = reachableNeighbors
            .filter(([neighborX, neighborY]) => distanceMap[neighborY][neighborX] > distanceFromStart);

        for (const [neighborX, neighborY] of unreachedNeighbors) {
            distanceMap[neighborY][neighborX] = distanceFromStart;
            coordinateQueue.push([neighborX, neighborY]);
        }
    };

    distanceMap[startY][startX] = 0;
    while (coordinateQueue.length > 0)
        calculateNeighbors(coordinateQueue.shift()!);
    
    return distanceMap[endY][endX];
};

const findCoordinate = (id: 'S' | 'E'): Coordinate => level
    .map(x => x.indexOf(id))
    .map((x, i) => [x, i] as Coordinate)
    .find(([x, y]) => x >= 0)!;

const distance = findPathDistance(findCoordinate('S'), findCoordinate('E'));
console.log(distance);
