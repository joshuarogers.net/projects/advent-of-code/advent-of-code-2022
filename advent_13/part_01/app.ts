import { readFileSync } from 'fs';
import { sum, zip } from 'lodash';

type Packet = (Packet | number)[];
type Pair = [Packet, Packet];

const pairs: Pair[] = readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n\n')
    .map(x => x.split('\n').map(y => JSON.parse(y)) as Pair);

enum PacketOrdering {
    Ordered,
    Unordered,
    Equal
}

const arePairsOrdered = ([firstPacket, secondPacket]: Pair): PacketOrdering => {
    const balancedPairs = zip(firstPacket, secondPacket);

    for (const [datum1, datum2] of balancedPairs) {
        if (datum1 === undefined)
            return PacketOrdering.Ordered;
        if (datum2 === undefined)
            return PacketOrdering.Unordered;

        const isFirstNumber = typeof datum1 === 'number';
        const isSecondNumber = typeof datum2 === 'number';
    
        if (isFirstNumber && isSecondNumber) {
            if (datum1 < datum2)
                return PacketOrdering.Ordered;
            if (datum1 > datum2)
                return PacketOrdering.Unordered;
        } else {
            const datumArray1 = isFirstNumber
                ? [ datum1 ]
                : datum1;
            const datumArray2 = isSecondNumber
                ? [ datum2 ]
                : datum2;

            const orderStatus = arePairsOrdered([datumArray1, datumArray2]);
            if (orderStatus !== PacketOrdering.Equal)
                return orderStatus;
        }
    }

    return PacketOrdering.Equal;
};

const orderedPairIds = pairs
    .map((x, i) => [arePairsOrdered(x), i + 1] as [PacketOrdering, number])
    .filter(([x, _]) => x === PacketOrdering.Ordered)
    .map(([_, i]) => i);

console.log(sum(orderedPairIds));