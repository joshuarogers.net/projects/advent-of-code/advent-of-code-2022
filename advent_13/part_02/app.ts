import { readFileSync } from 'fs';
import { sum, zip } from 'lodash';

type Packet = (Packet | number)[];
type Pair = [Packet, Packet];

const inputPacketText: string[] = readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n')
    .filter(x => x !== '');

const dividerPacketText = [ '[[2]]', '[[6]]' ];

enum PacketOrdering {
    Ordered = -1,
    Unordered = 1,
    Equal = 0
}

const arePairsOrdered = ([firstPacket, secondPacket]: Pair): PacketOrdering => {
    const balancedPairs = zip(firstPacket, secondPacket);

    for (const [datum1, datum2] of balancedPairs) {
        if (datum1 === undefined)
            return PacketOrdering.Ordered;
        if (datum2 === undefined)
            return PacketOrdering.Unordered;

        const isFirstNumber = typeof datum1 === 'number';
        const isSecondNumber = typeof datum2 === 'number';
    
        if (isFirstNumber && isSecondNumber) {
            if (datum1 < datum2)
                return PacketOrdering.Ordered;
            if (datum1 > datum2)
                return PacketOrdering.Unordered;
        } else {
            const datumArray1 = isFirstNumber
                ? [ datum1 ]
                : datum1;
            const datumArray2 = isSecondNumber
                ? [ datum2 ]
                : datum2;

            const orderStatus = arePairsOrdered([datumArray1, datumArray2]);
            if (orderStatus !== PacketOrdering.Equal)
                return orderStatus;
        }
    }

    return PacketOrdering.Equal;
};

const sortedPackets = inputPacketText
    .concat(dividerPacketText)
    .sort((packetText1, packetText2) => arePairsOrdered([JSON.parse(packetText1), JSON.parse(packetText2)]));

const decoderKey = dividerPacketText
    .map(x => sortedPackets.indexOf(x) + 1)
    .reduce((x, y) => x * y, 1);

console.log(decoderKey);
