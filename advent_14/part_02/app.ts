import { readFileSync,writeFileSync } from 'fs';
import { chain, isEqual, range, zip } from 'lodash';

type Coordinate = { x: number, y: number };
type Line = { vertices: Coordinate[] };
type BoundingBox = { minX: number, minY: number, maxX: number, maxY: number };

type LevelContent = 'air' | 'sand' | 'stone';

interface Level {
    getContent(coordinate: Coordinate): LevelContent | 'out of bounds';
    setContent(coordinate: Coordinate, content: LevelContent): void;
    boundingBox: BoundingBox;
}
type SandState = 'falling' | 'resting' | 'out of bounds';


interface Sand {
    getPosition: () => Coordinate;
    getState: () => SandState;
    tick: () => void;
}

const toPairs = <T>(items: T[]): [T, T][] => zip(items.slice(0, -1), items.slice(1)) as [T, T][];
const toBoundingBox = (coordinates: Coordinate[]): BoundingBox => {
    const xs = coordinates.map(x => x.x);
    const ys = coordinates.map(x => x.y);

    return {
        minX: Math.min(...xs),
        minY: Math.min(...ys),
        maxX: Math.max(...xs),
        maxY: Math.max(...ys)
    };
};

const parseLine = (lineText: string): Line => {
    const vertices = lineText
        .split(' -> ')
        .map(x => x.split(','))
        .map(([x, y]) => ({ x: parseInt(x), y: parseInt(y) }));
    
    return { vertices };
}

const rasterizeLine = (line: Line): Coordinate[] => {
    return chain(toPairs(line.vertices))
        .map(toBoundingBox)
        .map(({minX, minY, maxX, maxY}) => minX === maxX
            ? range(minY, maxY + 1).map(y => ({ x: minX, y }))
            : range(minX, maxX + 1).map(x => ({ x, y: minY })))
        .flatten()
        .uniqWith(isEqual)
        .value();
};

const createLevel = ({ minX, minY, maxX, maxY }: BoundingBox): Level => {
    const levelContents: LevelContent[][] = range(minY, maxY + 1)
        .map(() => range(minX, maxX + 1)
            .map(() => 'air'));

    const isCandidateInBounds = ({x, y}: Coordinate) => minX <= x
        && x < maxX
        && minY + 1 <= y
        && y < maxY + 1;

    return {
        getContent: ({x, y}) => isCandidateInBounds({x, y})
            ? levelContents[y - minY][x - minX]
            : 'out of bounds',
        setContent: ({x, y}, content) => { 
            if (isCandidateInBounds({x, y}))
                levelContents[y - minY][x - minX] = content;
        },
        boundingBox: { minX, minY, maxX, maxY }
    };
};

const createSandParticle = (level: Level, initialPosition: Coordinate): Sand => {
    let currentPosition = initialPosition;
    let state: SandState = 'falling';

    return {
        getPosition: () => currentPosition,
        getState: () => state,
        tick: () => {
            const candidatePositions: Coordinate[] = [
                { x: currentPosition.x, y: currentPosition.y + 1 },
                { x: currentPosition.x - 1, y: currentPosition.y + 1 },
                { x: currentPosition.x + 1, y: currentPosition.y + 1 }
            ];

            for (const candidatePosition of candidatePositions) {
                const existingContent = level.getContent(candidatePosition);

                if (existingContent === 'sand' || existingContent === 'stone')
                    continue;

                if (existingContent === 'out of bounds')
                    state = 'out of bounds';

                if (existingContent === 'air') {
                    level.setContent(currentPosition, 'air');
                    level.setContent(candidatePosition, 'sand');
                }

                currentPosition = candidatePosition;
                return;
            }

            state = 'resting';
        }
    };
};

const barrierCoordinates = readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n')
    .map(parseLine)
    .map(rasterizeLine)
    .flat();

const { minX, maxX, maxY } = toBoundingBox(barrierCoordinates)
const level = createLevel({ minX: minX - maxY - 2, maxX: maxX + maxY + 2, minY: 0, maxY: maxY + 2});

const visualizeLevel = (level: Level) => {
    const symbology: Record<ReturnType<Level['getContent']>, string> = {
        'air' : ' ',
        'sand' : '+',
        'stone' : '#',
        'out of bounds': '~'
    };

    const output = range(level.boundingBox.minY, level.boundingBox.maxY + 1)
        .map(y => range(level.boundingBox.minX, level.boundingBox.maxX)
            .map(x => symbology[level.getContent({ x, y })]).join('')).join('\n');
    
    writeFileSync('output.txt', output);
}

for (const stoneCoordinate of barrierCoordinates)
    level.setContent(stoneCoordinate, 'stone');

for (var x = level.boundingBox.minX; x < level.boundingBox.maxX; x++)
    level.setContent({ x, y: level.boundingBox.maxY }, 'stone');

const restingSandParticles: Sand[] = [];
let isSandFalling = true;
while (isSandFalling) {
    const sand = createSandParticle(level, { x: 500, y: 0 });
    while (sand.getState() === 'falling')
        sand.tick();
    
    restingSandParticles.push(sand);
    
    if (sand.getState() === 'resting' && sand.getPosition().y === 0)
        isSandFalling = false;
}

visualizeLevel(level);
console.log(restingSandParticles.length);