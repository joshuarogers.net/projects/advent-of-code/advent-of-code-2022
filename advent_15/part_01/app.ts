import { readFileSync } from 'fs';
import { chain, last, orderBy, range, sum, tail } from 'lodash';

interface Coordinate {
    x: number;
    y: number;
}

interface Beacon {
    position: Coordinate;
}

interface Sensor {
    position: Coordinate;
    nearestBeacon: Beacon;
}

type Range = [number, number];

const calculateDistance = (coordinate1: Coordinate, coordinate2: Coordinate): number =>
    Math.abs(coordinate2.x - coordinate1.x) + Math.abs(coordinate2.y - coordinate1.y);

const lineParseRegex = /Sensor at x=([-\d]+), y=([-\d]+): closest beacon is at x=([-\d]+), y=([-\d]+)/;
const parseLine = (line: string): Sensor => {
    const [sensorX, sensorY, beaconX, beaconY] = lineParseRegex.exec(line)?.slice(1)!;
    
    return {
        position: { x: parseInt(sensorX), y: parseInt(sensorY) },
        nearestBeacon: {
            position: { x: parseInt(beaconX), y: parseInt(beaconY) }
        }
    };
}

const getXRangeForY = (sensor: Sensor, y: number): Range | undefined => {
    const sensorDistanceToBeacon = calculateDistance(sensor.position, sensor.nearestBeacon.position);
    const sensorDistanceToY = calculateDistance(sensor.position, { x: sensor.position.x, y });
    const radiusAtY = sensorDistanceToBeacon - sensorDistanceToY;

    return radiusAtY <= 0
        ? undefined
        : [sensor.position.x - radiusAtY, sensor.position.x + radiusAtY];
};

const sortRanges = (ranges: Range[]): Range[] =>
    orderBy(ranges, ([start, end]) => start);

const mergeRanges = (ranges: Range[]): Range[] => {
    const orderedRanges = sortRanges(ranges);
    return orderedRanges.reduce((ranges, range) => {
        if (ranges.length === 0)
            return [range];
        
        const lastRange = last(ranges)!;
        
        // If there is at least one number between the last range and this one, they can't be combined.
        if (lastRange[1] + 1 < range[0])
            return [...ranges, range];

        lastRange[1] = Math.max(lastRange[1], range[1]);
        return ranges;
    }, [] as Range[]);
}

const removeFromRange = (ranges: Range[], value: number): Range[] => {
    const rangeWorkingSet = [...ranges];
    const targetRange = rangeWorkingSet.find(([start, end]) => start <= value && value <= end);

    if (!targetRange)
        return rangeWorkingSet;

    rangeWorkingSet.splice(rangeWorkingSet.indexOf(targetRange), 1);

    // If there is a value being removed from a range with a size of one, the range no longer exists.
    if (targetRange[0] === targetRange[1])
        return rangeWorkingSet;

    if (targetRange[0] === value)
        rangeWorkingSet.push([targetRange[0] + 1, targetRange[1]]);
    else if (targetRange[1] === value)
        rangeWorkingSet.push([targetRange[0], targetRange[1] - 1]);
    else {
        rangeWorkingSet.push([targetRange[0], value - 1]);
        rangeWorkingSet.push([value + 1, targetRange[1]]);
    }

    return sortRanges(rangeWorkingSet);
}

const input = readFileSync('input.txt')
    .toString()
    .trim()
    .split('\n')
    .map(parseLine);

const y = 2000000;

const beaconXs = input
    .map(x => x.nearestBeacon.position)
    .filter(x => x.y === y)
    .map(x => x.x);

const ranges = input
    .map(x => getXRangeForY(x, y))
    .filter(x => x) as Range[];

const mergedRanges = mergeRanges(ranges);
const rangesUnableToContainBeacon = beaconXs
    .reduce((ranges, x) => removeFromRange(ranges, x), mergedRanges);

console.log(sum(rangesUnableToContainBeacon
    .map(([min, max]) => 1 + max - min)));